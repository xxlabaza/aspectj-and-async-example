package ru.xxlabaza.test.aspectj;

import lombok.Data;

/**
 *
 * @author Artem Labazin
 * <p>
 * @since Jan 15, 2016 | 5:24:56 PM
 * <p>
 * @version 1.0.0
 */
@Data
class MyPojo {

    private String name;

    private int age;
}
