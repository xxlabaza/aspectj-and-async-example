package ru.xxlabaza.test.aspectj;

/**
 *
 * @author Artem Labazin
 * <p>
 * @since Jan 14, 2016 | 5:49:25 PM
 * <p>
 * @version 1.0.0
 */
public class NotBean {

    public void doSomeUseful (String text) {
        // some useful logic
    }
}
